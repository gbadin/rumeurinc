package verbale.epidemie;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AlgoCreationCommunauteEnhanced implements IAlgoCreationNetwork {

    public static final float DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE_MIN = 0.5f;
    public static final float DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE_MAX = 1.0f;
    public static final int DEFAULT_CONNECTIVITY_OUT_COMMUNAUTE_MIN = 1;
    public static final int DEFAULT_CONNECTIVITY_OUT_COMMUNAUTE_MAX = 5;

    private int numMember;
    private int numCommunaute;

    private float connectivityProbabilityInCommunauteMax;
    private float connectivityProbabilityInCommunauteMin;

    private int connectivityOutCommunauteMax;
    private int connectivityOutCommunauteMin;

    public AlgoCreationCommunauteEnhanced(int numMember, int numCommunaute, float connectivityProbabilityInCommunauteMax, float connectivityProbabilityInCommunauteMin, int connectivityOutCommunauteMax, int connectivityOutCommunauteMin) {
        this.numMember = numMember;
        this.numCommunaute = numCommunaute;
        this.connectivityProbabilityInCommunauteMax = connectivityProbabilityInCommunauteMax;
        this.connectivityProbabilityInCommunauteMin = connectivityProbabilityInCommunauteMin;
        this.connectivityOutCommunauteMax = connectivityOutCommunauteMax;
        this.connectivityOutCommunauteMin = connectivityOutCommunauteMin;
    }

    public AlgoCreationCommunauteEnhanced(int numMember, int numCommunaute) {
        this.numMember = numMember;
        this.numCommunaute = numCommunaute;
        this.connectivityProbabilityInCommunauteMax = DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE_MAX;
        this.connectivityProbabilityInCommunauteMin = DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE_MIN;
        this.connectivityOutCommunauteMax = DEFAULT_CONNECTIVITY_OUT_COMMUNAUTE_MAX;
        this.connectivityOutCommunauteMin = DEFAULT_CONNECTIVITY_OUT_COMMUNAUTE_MIN;
    }

    @Override
    public Graph createGraph(Graph network) {
        if(numMember > numCommunaute) {

            List<Communaute> communautes = new ArrayList<Communaute>(numCommunaute);
            int moyenneCommunaute = Math.round((float) numMember / numCommunaute);
            for (int i = 0; i < numMember; i++) {
                Node n = network.addNode("" + i);
                n.addAttribute("rumor", new PeopleAttribute(n));
            }

            List<Node> nodeList = new ArrayList<Node>(network.getNodeSet().size());
            for(Node n : network.getEachNode()) {
                nodeList.add(n);
            }

            Random r = new Random();

            for (int i = 0; i < numCommunaute ; i++) {
                Communaute com = new Communaute();
                int tailleCommunaute = (int)Math.round((Math.random()+0.5) * moyenneCommunaute);
                List<Node> nodeCopy = new ArrayList<Node>(nodeList);
                for(int j = 0; j < tailleCommunaute; j ++)
                {
                    com.addNodes(nodeCopy.remove((int)Math.floor(Math.random()*nodeCopy.size())));
                }

                for(int j = 0; j < com.getMembres().size(); j ++)
                {
                    List<Node> copyListNodeCopy = nodeCopy;
                    List<Node> copyListCom = com.getMembres();
                    Node n = copyListCom.get(j);
                    copyListCom.remove(n);

                    int maxEdgeIn4Com = (int) Math.round(connectivityProbabilityInCommunauteMax * (copyListCom.size()));
                    int minEdgeIn4Com = (int) Math.round(connectivityProbabilityInCommunauteMin * (copyListCom.size()));

                    int numEdgeIn4N;

                    if(maxEdgeIn4Com == minEdgeIn4Com)
                        numEdgeIn4N = r.nextInt(maxEdgeIn4Com) + minEdgeIn4Com;
                    else
                        numEdgeIn4N = r.nextInt(maxEdgeIn4Com - minEdgeIn4Com) + minEdgeIn4Com;

                    int numEdgeOut4N;

                    numEdgeOut4N = r.nextInt(connectivityOutCommunauteMax - connectivityOutCommunauteMin) + connectivityOutCommunauteMin;

                    for(int k = 0; k < numEdgeIn4N; k++)
                    {
                        Node n2 = copyListCom.get(r.nextInt(copyListCom.size()));
                        if(!n.hasEdgeBetween(n2))
                            network.addEdge(n.getId() +"--"+ n2.getId(), n, n2).addAttribute("lien",new RelationAttribute());
                        copyListCom.remove(n2);
                    }

                    for(int k = 0; k < numEdgeOut4N; k++) {
                        Node n2 = copyListNodeCopy.get(r.nextInt(copyListNodeCopy.size()));
                        if(!n.hasEdgeBetween(n2))
                            network.addEdge(n.getId() +"--"+ n2.getId(), n, n2).addAttribute("lien",new RelationAttribute());
                        copyListNodeCopy.remove(n2);
                    }
                }

                communautes.add(com);
            }

            for(int i = 0; i < nodeList.size(); i++)
            {
                int randomEdges4All = r.nextInt(6) + 1;
                Node n = nodeList.get(r.nextInt(nodeList.size()));
                boolean edgeCreated = false;
                for(int k = 0; k < randomEdges4All; k++) {
                    edgeCreated = false;
                    do {
                        Node n2 = nodeList.get(r.nextInt(nodeList.size()));
                        if (!n.hasEdgeBetween(n2)) {
                            network.addEdge(n.getId() + "--" + n2.getId(), n, n2).addAttribute("lien",new RelationAttribute());
                            edgeCreated = true;
                        }
                    }while(!edgeCreated);
                }
            }
        }
        return network;
    }
}
