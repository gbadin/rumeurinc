package verbale.epidemie;

import org.graphstream.graph.Graph;

public interface IAlgoCreationNetwork {

    public Graph createGraph(Graph network);
}
