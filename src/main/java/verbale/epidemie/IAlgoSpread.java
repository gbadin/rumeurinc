package verbale.epidemie;

import org.graphstream.graph.Graph;

public interface IAlgoSpread {

    public static final int DELAY_BETWEEN_PROPAGATION = 1500;

    public void simulateSpread(Graph network, RumorAttribute rumor);
}
