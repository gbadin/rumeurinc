package verbale.epidemie;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

public class AlgoCreationNetworkCommunauteSimple implements IAlgoCreationNetwork{


    public static final float DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE = 0.5f;
    public static final int DEFAULT_CONNECTIVITY_AVERAGE_OUT_COMMUNAUTE = 5;

    private int numMember;
    private int numCommunaute;

    private float connectivityProbabilityInCommunaute;
    private float connectivityProbabilityOutCommunaute;

    public AlgoCreationNetworkCommunauteSimple(int numMember, int numCommunaute) {
        this.numMember = numMember;
        this.numCommunaute = numCommunaute;
        connectivityProbabilityInCommunaute = DEFAULT_CONNECTIVITY_PROBABILITY_IN_COMMUNAUTE;
        connectivityProbabilityOutCommunaute = (float)DEFAULT_CONNECTIVITY_AVERAGE_OUT_COMMUNAUTE/numMember;
    }

    public AlgoCreationNetworkCommunauteSimple(int numMember, int numCommunaute, float connectivityProbabilityInCommunaute, int connectivityAverageOutCommunaute) {
        this.numMember = numMember;
        this.numCommunaute = numCommunaute;
        this.connectivityProbabilityInCommunaute = connectivityProbabilityInCommunaute;
        this.connectivityProbabilityOutCommunaute = (float)connectivityAverageOutCommunaute/numMember;
    }

    @Override
    public Graph createGraph(Graph network) {

        if(numMember > numCommunaute) {

            List<Communaute> communautes = new ArrayList<Communaute>(numCommunaute);
            int moyenneCommunaute = Math.round((float) numMember / numCommunaute);
            for (int i = 0; i < numMember; i++) {
                Node n = network.addNode("" + i);
                n.addAttribute("rumor", new PeopleAttribute(n));
            }

            List<Node> nodeList = new ArrayList<Node>(network.getNodeSet().size());
            for(Node n : network.getEachNode()) {
                nodeList.add(n);
            }

            for (int i = 0; i < numCommunaute ; i++) {
                Communaute com = new Communaute();
                int tailleCommunaute = (int)Math.round((Math.random()+0.5) * moyenneCommunaute);
                List<Node> nodeCopy = new ArrayList<Node>(nodeList);
                for(int j = 0; j < tailleCommunaute; j ++)
                {
                    com.addNodes(nodeCopy.remove((int)Math.floor(Math.random()*nodeCopy.size())));
                }

                for(int j = 0; j < com.getMembres().size(); j ++)
                {
                    for(int k = j+1; k < com.getMembres().size(); k ++)
                    {
                        Node n = com.getMembres().get(j);
                        Node n2 = com.getMembres().get(k);
                        if(!n.hasEdgeBetween(n2) && Math.random() < connectivityProbabilityInCommunaute) {
                            network.addEdge(n.getId()+"-"+n2.getId(),n,n2);
                        }
                    }
                }
                communautes.add(com);
            }

            for(int i = 0; i < nodeList.size(); i++) {
                for(int j = i+1; j < nodeList.size() ;j++) {
                    Node n = nodeList.get(i);
                    Node n2 = nodeList.get(j);
                    if(!n.hasEdgeBetween(n2) && Math.random() < connectivityProbabilityOutCommunaute) {
                        network.addEdge(n.getId()+"-"+n2.getId(),n,n2);
                    }
                }
            }
        }
        return network;
    }
}
