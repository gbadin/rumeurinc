package verbale.epidemie;


import org.graphstream.graph.Graph;
import org.graphstream.graph.GraphFactory;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

public class SocialNetwork {

    private Graph network;
    private IAlgoCreationNetwork algo = new AlgoCreationCommunauteEnhanced(100,20);
    private IAlgoSpread spread = new BasicAlgoSpread(1500);

    public SocialNetwork() {
        GraphFactory factory = new GraphFactory();
        network = factory.newInstance("Réseau social","SingleGraph");
    }

    public void initGraphe() {
        network = algo.createGraph(network);
        network.display();
    }

    public void startAlgoRumeur() {
        RumorAttribute rumor = new RumorAttribute();
        spread.simulateSpread(network,rumor);
    }
}
