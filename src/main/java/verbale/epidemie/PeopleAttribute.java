package verbale.epidemie;

import org.graphstream.graph.Node;

import java.util.Random;

public class PeopleAttribute {

    public static final String PROPAGATE_STYLE = "fill-color: rgb(255,0,0);";
    public static final String KNOW_RUMOR_STYLE = "fill-color: rgb(155,0,255);";
    public static final String NOT_CONTAMINATED_STYLE = "fill-color: rgb(0,0,255);";
    public static final String DENIAL_STYLE = "fill-color: rgb(155,255,0);";

    private float gullibleDegree;

    private State state;
    private Node self;
    private boolean knowDenial;

    public enum State{
        NOT_KNOWING,KNOWING,PROPAGATING,DENYING
    }

    public PeopleAttribute(Node n){

        state = State.NOT_KNOWING;

        self = n;

        knowDenial = false;

        Random r = new Random();

        gullibleDegree = r.nextFloat();
        refreshColor();
    }

    public void setState(State state) {
        this.state = state;
        refreshColor();
    }

    public void setKnowDenial(boolean b)
    {
        knowDenial = b;
    }

    public boolean isKnowDenial()
    {
        return knowDenial;
    }

    public State getState() {
        return state;
    }

    private void refreshColor()
    {
        switch(state)
        {
            case NOT_KNOWING:
                self.addAttribute("ui.style", NOT_CONTAMINATED_STYLE);
                break;
            case KNOWING:
                self.addAttribute("ui.style", KNOW_RUMOR_STYLE);
                break;
            case PROPAGATING:
                self.addAttribute("ui.style", PROPAGATE_STYLE);
                break;
            case DENYING:
                self.addAttribute("ui.style", DENIAL_STYLE);
                break;
        }
    }

    public float getGullibleDegree(){ return gullibleDegree;}
}
