package verbale.epidemie;

import java.util.Random;

/**
 * Created by Exphoria on 03/02/2015.
 */
public class RumorAttribute {

    private float credibility;

    public RumorAttribute(){
        Random r = new Random();

        credibility = r.nextFloat();
    }

    public float getCredibility() { return credibility;}

}
