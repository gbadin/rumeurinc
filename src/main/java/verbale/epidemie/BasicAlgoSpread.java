package verbale.epidemie;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class BasicAlgoSpread implements IAlgoSpread{

    private int delayBetweenTurns = DELAY_BETWEEN_PROPAGATION;

    public BasicAlgoSpread() {
    }

    public BasicAlgoSpread(int delayBetweenTurns) {

        this.delayBetweenTurns = delayBetweenTurns;
    }

    @Override
    public void simulateSpread(Graph network, RumorAttribute rumor) {

        int start = (int)Math.floor(Math.random()*network.getNodeCount());
        Set<Node> infectes = new HashSet<Node>();
        Node startNode = network.getNode(start);
        ((PeopleAttribute)startNode.getAttribute("rumor")).setState(PeopleAttribute.State.PROPAGATING);
        infectes.add(startNode);

        Random r = new Random();

        boolean isInfection = false;
        do {
            isInfection = false;

            try {
                Thread.sleep(delayBetweenTurns);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Set<Node> newInfectes = new HashSet<Node>();

            for(Node n : infectes) {
                PeopleAttribute p = (PeopleAttribute)n.getAttribute("rumor");
                if(p.getState() == PeopleAttribute.State.PROPAGATING) {
                    for (Edge e : n.getEachEdge()) {
                        Node n2 = e.getOpposite(n);
                        PeopleAttribute p2 = (PeopleAttribute) n2.getAttribute("rumor");
                        if (p2.getState() == PeopleAttribute.State.NOT_KNOWING) {
                            if (r.nextInt(100) < rumor.getCredibility())
                                p2.setState(PeopleAttribute.State.PROPAGATING);
                            else
                                p2.setState(PeopleAttribute.State.KNOWING);
                            infectes.add(n2);
                            isInfection = true;
                        }
                    }
                }
            }
            infectes.addAll(newInfectes);

        }while (isInfection);
    }
}
