package verbale.epidemie;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import java.util.Random;

public class RelationAttribute {

    private float trust0;
    private float trust1;

    public RelationAttribute(){
        Random r = new Random();

        trust0 = r.nextFloat();
        trust1 = r.nextFloat();
    }

    public float getTrust0() { return trust0;}
    public float getTrust1() { return trust1;}

    public float getTrust(Edge e, Node n)
    {
        if(e.getNode0() == n)
            return trust0;
        else if (e.getNode1() == n)
            return trust1;
        return 0;
    }
}
