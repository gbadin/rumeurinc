package verbale.epidemie;

import org.graphstream.graph.Graph;
import org.graphstream.graph.GraphFactory;

public class System
{
    public static void main (String args[]) {
        SocialNetwork network = new SocialNetwork();
        network.initGraphe();
        network.startAlgoRumeur();
    }
}