package verbale.epidemie;

import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

public class Communaute {
    private List<Node> membres;

    public Communaute (){
        membres = new ArrayList<Node>();
    }

    public void addNodes(Node node){
        membres.add(node);
    }

    public void removeNodes(Node node){
        membres.remove(node);
    }

    public List<Node> getMembres() {
        return membres;
    }
}
